---
title: Spinach Dip
date: 2019-11-10
---

## Ingredients:

* 1 PKG Frozen Chopped Spinach
* 1 PKG Knorr Vegetable and Soup
* 1 Cup Sour Cream
* 1 Cup Mayonnaise
* 1 Tablespoon Dried Onions
* 1 Cup wWater Chestnuts Chopped

## Directions: 

1. Drain chopped spinnach
2. Mix all ingredients together
3. Use crackers, vegetables, or sweet breads